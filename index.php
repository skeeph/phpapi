<?php
include __DIR__ . '/vendor/autoload.php';
include './utils.php';
include './config.php';
include './dbHelpers.php';


include './resources/Nodes.php';
include './resources/Products.php';
include './resources/Shops.php';
include './resources/ProductLinks.php';
include './resources/User.php';
include './resources/Actions.php';


$app = new \Slim\Slim(array(
    'templates.path' => './templates'

));

$options = array('dir' => './storageData');
$stor = new \Flintstone\Flintstone('stor', $options);

function APIrequest()
{
    $app = \Slim\Slim::getInstance();
    $app->view(new \JsonApiView("data", "meta"));
    $app->add(new \JsonApiMiddleware());
}

function register()
{

    $request = \Slim\Slim::getInstance()->request();
    $user = json_decode($request->getBody());
    $sql = "INSERT INTO users (fname, surname, patronymic,email,password, username) VALUES (:fname, :surname, :patronymic,:email,:password, :username)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(":fname", $request->params('name'));
        $stmt->bindParam(":surname", $request->params('surname'));
        $stmt->bindParam(":password", hashPass($request->params('password')));
        $stmt->bindParam(":email", $request->params('email'));
        $stmt->bindParam(":patronymic", $request->params('patronymic'));
        $stmt->bindParam(":username", $request->params('username'));
        $stmt->execute();
        #$user->id = $db->lastInsertId();
        $db = null;


        print('{"success":1}');

    } catch (PDOException $e) {
        error_log($e->getMessage(), 3, '/var/tmp/php.log');
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}

$app->post('/user', register);


function auth()
{
    $app = \Slim\Slim::getInstance();
    $request = $app->request;

    $username = filter_var($request->post('username'), FILTER_SANITIZE_STRING);
    $password = filter_var($request->post('password'), FILTER_SANITIZE_STRING);
    if (strlen($username) == 0 or strlen($password) == 0) {
        $app->render(200, array('msg' => 'provide username and password'));
    } else {
        $sql = "SELECT * FROM users 
                    WHERE username = :username AND password = :password";
        $user = false;
        try {
            $db = getConnection();
            $db->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES cp1251');
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $stmt = $db->prepare($sql);
            $stmt->bindParam(':username', $username, PDO::PARAM_STR);
            $stmt->bindParam(':password', hashPass($password), PDO::PARAM_STR);

            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                $user = $stmt->fetchAll()[0];
            } else {
                header('Content-type:application/json');
                echo json_encode(array('Error' => true, 'msg' => 'User not found or invalid pass'));
                exit(401);
            }
        } catch (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            header('Content-type:application/json');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
            exit(401);
        }
        $user = new User($user['id']);
        $jwt = $user->getToken();
        $response = array('token' => $jwt);
        $app->render(200, $response);
    }


}

$app->get('/auth', function () use ($app) {
    $app->render('login.html');
});

$app->post('/auth', 'APIrequest', auth);


$app->get('/', 'APIrequest', function () use ($app) {
    $app->render(200, array("now" => time()));
});

$app->get('/goods', 'APIrequest', function () use ($app) {
    $timestamp = $app->request->get('timestamp');
    if (strlen($timestamp) == 0) {
        $app->render(403, array('error' => 'Timestamp argument is required'));
        exit(403);
    }
    $datetime = strToDatetime($timestamp);
    $ts = $datetime->format("Y-m-d H:i:s");
    $res = array();

    $res['deleted'] = array();
    $res['new'] = array();
    $res['changed'] = array();

    $nodes = new Nodes();
    $products = new Products();
    $shops = new Shops();
    $product_links = new ProductLinks();

    $res['timestamp'] = $ts;
    $res['deleted']['nodes'] = $nodes->get_deleted($ts);
    $res['deleted']['products'] = $products->get_deleted($ts);
    $res['deleted']['shops'] = $shops->get_deleted($ts);
    $res['deleted']['productLinks'] = $product_links->get_deleted($ts);

    $res['new']['nodes'] = $nodes->get_new($ts);
    $res['new']['shops'] = $shops->get_new($ts);
    $res['new']['products'] = $products->get_new($ts);
    $res['new']['productLinks'] = $product_links->get_new($ts);

    $res['changed']['nodes'] = $nodes->get_changed($ts);
    $res['changed']['shops'] = $shops->get_changed($ts);
    $res['changed']['products'] = $products->get_changed($ts);
    $res['changed']['productLinks'] = $product_links->get_changed($ts);

    if (array_size($res) > THRESHOLD) {
        $app->render(200, chunkArray($res));
    } else {
        $res['next'] = false;
        $app->render(200, $res);
    }
});

$app->get('/getNext', 'APIrequest', function () use ($app) {
    $token = $app->request->get('token');
    if (strlen($token) == 0) {
        $app->render(403, array('error' => 'token argument is required'));
        exit(403);
    }
    global $stor;
    $arr = $stor->get($token);
    $stor->delete($token);
    if ($arr) {
        $app->render(200, chunkArray($arr));
    } else {
        $app->render(401, array('error' => true, 'msg' => 'invalid token'));
    }

});

$app->get('/register', function () use ($app) {
    $app->render('register.html');
});

$app->post('/list', 'APIrequest', function () use ($app) {
    $json = $app->request->getBody();
    $data = json_decode($json, true);

    $user = User::validate_token(checkKey());
    //Вставка новых списокв
    try {

        $db = getConnection();
        $db->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES cp1251');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        if (array_key_exists('new', $data)) {
            foreach ($data['new'] as $new) {
                $sql = 'INSERT INTO product_lists (userId,name) VALUES (:userId,:pname)';
                $id = $user->id;
                $stmt = $db->prepare($sql);
                $stmt->bindValue(':pname', filter_var(iconv('utf8', 'cp1251', $new['name']), FILTER_SANITIZE_STRING));
                $stmt->bindValue(':userId', filter_var($id, FILTER_SANITIZE_STRING));
                $stmt->execute();
                $id = $db->lastInsertId();

                $products = $new['products'];
                $query = "INSERT INTO list_product (listId, productId) VALUES "; //Prequery
                $qPart = array_fill(0, count($products), "(?, ?)");
                $query .= implode(",", $qPart);
                $stmt = $db->prepare($query);
                $i = 1;
                foreach ($products as $item) { //bind the values one by one
                    $stmt->bindValue($i++, $id);
                    $stmt->bindValue($i++, $item);
                }
                $stmt->execute(); //execute
            }
        }
        if (array_key_exists('delete', $data)) {
            $delete = $data['delete'];
            foreach ($delete as $deleteId) {
                $delete_products_query = 'DELETE FROM `list_product` WHERE `listId`=:listId;';
                $stmt = $db->prepare($delete_products_query);
                $stmt->bindValue(':listId', $deleteId);
                $stmt->execute();
                $delete_list_query = 'DELETE FROM `product_lists` WHERE `id`=:listId;';
                $stmt = $db->prepare($delete_list_query);
                $stmt->bindValue(':listId', $deleteId);
                $stmt->execute();
            }
        }


        if (array_key_exists('change', $data)) {
            foreach ($data['change'] as $change) {
                $update_products_query = 'UPDATE `product_lists` SET `name`=:chname WHERE `id`=:id';
                $stmt = $db->prepare($update_products_query);
                $stmt->bindValue(':chname', $change['name']);
                $stmt->bindValue(':id', $change['id']);
                $stmt->execute();
                $delete_list_query = 'DELETE FROM `list_product` WHERE `listId`=:listId;';
                $stmt = $db->prepare($delete_list_query);
                $stmt->bindValue(':listId', $change['id']);
                $stmt->execute();

                $products = $change['products'];
                $query = "INSERT INTO list_product (listId, productId) VALUES "; //Prequery
                $qPart = array_fill(0, count($products), "(?, ?)");
                $query .= implode(",", $qPart);
                $stmt = $db->prepare($query);
                $i = 1;
                foreach ($products as $item) { //bind the values one by one
                    $stmt->bindValue($i++, $change['id']);
                    $stmt->bindValue($i++, $item);
                }
                $stmt->execute();
            }
        }

    } catch
    (PDOException $e) {
        error_log($e->getMessage(), 3, '/var/tmp/php.log');
        echo '{"error":{"text":' . $e->getFile() . ' ' . $e->getLine() . ' ' . $e->getMessage() . '}}';
    }
    $app->render(200, ['ok' => true]);
});

$app->get('/user', 'APIrequest', function () use ($app) {
    $token = $app->request->get('token');
    if (strlen($token) == 0) {
        $app->render(403, array('error' => 'token argument is required'));
        exit(403);
    }
    $user = User::validate_token($token);
    $res = $user->getInfo();
    $res['shopList'] = $user->getList();
    $res['myShops'] = $user->getEmotions();
    $app->render(200, $res);
});

//Изменение списка любимых магазинов
$app->post('/emotions', function () use ($app) {
    $user = User::validate_token(checkKey());
    $id = $user->id;
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    if (array_key_exists('changed', $data)) {
        $sqlInsert = 'INSERT INTO shop_emotions(userId,shopId, emotion) VALUES (:userId,:shopId, :emotion)';
        $sqlDelete = 'DELETE FROM shop_emotions WHERE userId=:userId AND  shopId=:shopId';

        try {
            $db = getConnection();
            foreach ($data['changed'] as $item) {
                $stmt = $db->prepare($sqlDelete);
                $stmt->bindValue('userId', $id);
                $stmt->bindValue('shopId', $item['shopId']);
                $stmt->execute();

                $stmt = $db->prepare($sqlInsert);
                $stmt->bindValue('userId', $id);
                $stmt->bindValue('shopId', $item['shopId']);
                $stmt->bindValue('emotion', $item['emotion']);
                $stmt->execute();
            }
        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            $app->render(500, array('error' => $e->getMessage()));
            exit(500);
        }
    }
});

$app->post('/shopRating', function () use ($app) {
    $user = User::validate_token(checkKey());
    $id = $user->id;
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    $sql = 'INSERT INTO `shop_rating`(`userId`, `shopId`, `rating`) VALUES (:userId, :shopId, :rating)';
    $sqlDelete = 'DELETE FROM `shop_rating` WHERE `userId`=:userId AND `shopId`=:shopId';
    try {
        $db = getConnection();
        $stmt = $db->prepare($sqlDelete);
        $stmt->bindValue('userId', $id);
        $stmt->bindValue('shopId', $data['shopId']);
        $stmt->execute();

        $stmt = $db->prepare($sql);
        $stmt->bindValue('userId', $id);
        $stmt->bindValue('shopId', $data['shopId']);
        $stmt->bindValue('rating', $data['rating']);
        $stmt->execute();
        echo $db->lastInsertId();
    } catch
    (PDOException $e) {
        error_log($e->getMessage(), 3, '/var/tmp/php.log');
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
});

$app->post('/productRating', function () use ($app) {
    $id = User::validate_token(checkKey())->id;
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    $sql = 'INSERT INTO `product_rating`(`userId`, `productId`, `rating`) VALUES (:userId, :productId, :rating)';
    $sqlDelete = 'DELETE FROM `product_rating` WHERE `userId`=:userId AND `productId`=:productId';

    try {
        $db = getConnection();
        $stmt = $db->prepare($sqlDelete);
        $stmt->bindValue('userId', $id);
        $stmt->bindValue('productId', $data['productId']);
        $stmt->execute();

        $stmt = $db->prepare($sql);
        $stmt->bindValue('userId', $id);
        $stmt->bindValue('productId', $data['productId']);
        $stmt->bindValue('rating', $data['rating']);
        $stmt->execute();

        echo $db->lastInsertId();
    } catch
    (PDOException $e) {
        error_log($e->getMessage(), 3, '/var/tmp/php.log');
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }

});

$app->post('/changePrice', 'APIrequest', function () use ($app) {
    $id = User::validate_token(checkKey())->id;
    $json = $app->request->getBody();
    $data = json_decode($json, true);
    $res = array();
    try {
        $db = getConnection();
        $stmt = $db->prepare('SELECT `cost` FROM `product_links` WHERE `shopId`=:shopId AND `productId`=:productId');
        $stmt->bindValue('shopId', $data['shopId']);
        $stmt->bindValue('productId', $data['productId']);
        $stmt->execute();
        if ($stmt->rowCount() > 0) {
            $sql = 'UPDATE `product_links` SET `cost`=:cost,`changed`=CURRENT_TIMESTAMP WHERE `shopId`=:shopId AND `productId`=:productId';
        } else {
            $sql = 'INSERT INTO `product_links`(`productId`, `shopId`, `cost`) VALUES (:productId,:shopId,:cost);';
        }
        $stmt = $db->prepare($sql);
        $stmt->bindValue('shopId', $data['shopId']);
        $stmt->bindValue('productId', $data['productId']);
        $stmt->bindValue('cost', $data['cost']);
        $stmt->execute();
        $res['lastId'] = $db->lastInsertId();
    } catch (PDOException $e) {
        error_log($e->getMessage(), 3, '/var/tmp/php.log');
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
    $app->render(200, $res);
});

$app->get('/actions', 'APIrequest', function () use ($app) {
    $act = new Actions();
    $res = $act->get_discount_new();
    $app->render(200, $res);
});


$app->post('/push', 'APIrequest', function () use ($app) {
    $request = $app->request;
    $message = $request->get('message');
    $title = $request->get('title');
    $subtitle = $request->get('subtitle');
    $ticker = $request->get('ticker');
    $res = send_push($title, $subtitle, $message, $ticker);
    $app->render(200, $res);
});


$app->get('/openid', function () use ($app) {
    $app->render('openid.html');
});

$app->get('/openid_auth', 'APIrequest', function () use ($app) {
    $identity = $app->request->get('openid');
    $res = array();
    try {
        # Change 'localhost' to your domain name.
        $openid = new LightOpenID('localhost/openid_auth');
        if (!$openid->mode) {
            $openid->identity = $identity;
            # The following two lines request email, full name, and a nickname
            # from the provider. Remove them if you don't need that data.
            $openid->required = array('contact/email');
            $openid->optional = array('namePerson', 'namePerson/friendly');
            $app->redirect($openid->authUrl());
        } elseif ($openid->mode == 'cancel') {
            echo 'User has canceled authentication!';
        } else {
            $res = $openid->getAttributes();
        }
    } catch (ErrorException $e) {
        echo $e->getMessage();
    }

    $na = explode(' ', $res['namePerson']);
    $user = User::constructByEmail($res['contact/email']);
    $user->set_name($na[0]);
    $user->set_surname($na[1]);
    $user->save();
    $app->render(200, array('token' => $user->getToken()));
});

$app->get('/getBanner', 'APIrequest', function () use ($app) {
    $request = $app->request;
    $id = $request->get('id');
    $res = array('banner' => 'http://rfweb.ru/assets/images/kb/160f896e4725f47bdce1873f63c164ae.jpg');
    $app->render(200, $res);
});


$app->run();

