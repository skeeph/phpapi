<?php


class ProductLinks
{
    private $sql_new = 'SELECT `productId`,`shopId`,`cost` FROM `product_links` WHERE `added`>:ts';
    private $sql_changed = 'SELECT `productId`,`shopId`,`cost` FROM `product_links` WHERE `changed`>:ts';
    private $sql_delete = 'SELECT `shopId`,`productId` FROM `deleted_product_links` WHERE `timestamp`>:ts';

    function __construct()
    {
        try {
            $this->db = getConnection();
        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
    }

    function __destruct()
    {
        $this->db = null;
    }

    function get_new($ts)
    {
        $res = array();
        try {
            $stmt = $this->db->prepare($this->sql_new);
            $stmt->bindParam(':ts', $ts);
            $stmt->execute();
            $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        return $res;
    }

    function get_changed($ts)
    {
        $res = array();
        try {
            $stmt = $this->db->prepare($this->sql_changed);
            $stmt->bindParam(':ts', $ts);
            $stmt->execute();
            $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        return $res;
    }

    function get_deleted($ts)
    {
        $res = array();
        try {
            $statement = $this->db->prepare($this->sql_delete);
            $statement->bindValue(':ts', $ts);
            $statement->execute();
            foreach ($statement->fetchAll(PDO::FETCH_ASSOC) as $item) {
                array_push($res, array('shopId' => $item['shopId'], 'productId' => $item['productId']));
            };
        } catch (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        return $res;
    }
}