<?php

/**
 * Created by PhpStorm.
 * User: skeeph
 * Date: 4/9/16
 * Time: 8:05 PM
 */
class Nodes
{
    private $sqlDelete = 'SELECT `nodeId` FROM `deleted_nodes` WHERE `timestamp`>:ts';
    private $sqlChanged = 'SELECT `categoryID`,`name`,`description`,`picture`,`parent` FROM `price_categories` WHERE `changed`>:changed';
    private $sqlNew = 'SELECT `categoryID`,`name`,`description`,`picture`,`parent` FROM `price_categories` WHERE `added`>:added';

    function __construct()
    {
        $this->db = getConnection();
    }

    function __destruct()
    {
        $this->db = null;
    }

    public function get_new($ts)
    {
        $res = array();
        //Получение новых и измененных категорий(узлов)
        try {
            $stmt = $this->db->prepare($this->sqlNew);
            $stmt->bindValue(':added', $ts);
            $stmt->execute();
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $item) {
                array_push($res, array(
                    'id' => $item['categoryID'],
                    'name' => to_utf($item['name']),
                    'info' => to_utf($item['description']),
                    'image' => $item['picture'],
                    'parentId' => $item['parent']));
            };


        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        return $res;
    }

    public function get_changed($ts)
    {
        $res = array();
        try {
            $stmt = $this->db->prepare($this->sqlChanged);
            $stmt->bindValue(':changed', $ts);
            $stmt->execute();
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $item) {
                array_push($res, array(
                    'id' => $item['categoryID'],
                    'name' => to_utf($item['name']),
                    'info' => to_utf($item['description']),
                    'image' => $item['picture'],
                    'parentId' => $item['parent']));
            };
        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        return $res;
    }

    public function get_deleted($ts)
    {
        $res = array();
        try {
            //Получение удаленных узлов
            $statement = $this->db->prepare($this->sqlDelete);
            $statement->bindValue(':ts', $ts);
            $statement->execute();
            foreach ($statement->fetchAll(PDO::FETCH_ASSOC) as $item) {
                array_push($res, $item['nodeId']);
            };
        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        return $res;
    }
}