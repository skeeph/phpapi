<?php

class Actions
{
    private $sql_discount = "SELECT `shopId`,`productId`,`description`,`oldCost`,`newCost`,`beginTime`,`endTime` FROM `actions_discount`";
    private $sql_discount_new = "SELECT `productID`,`categoryID`,`name`,`Price`,`list_price`,`magazinID`,`datestart`,`dateend` FROM `price_products_sale` WHERE `datestart`<:now AND `dateend`>:now";
    private $sql_gift = "SELECT `id`,`shopId`,`description`,`beginTime`,`endTime` FROM `actions_gift`";

    function __construct()
    {
        try {
            $this->db = getConnection();
        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
    }

    function __destruct()
    {
        $this->db = null;
    }


    function get_discount_new()
    {
        $res = [];
        try {
            $stmt = $this->db->prepare($this->sql_discount_new);
            $stmt->bindValue('now', time());
            $stmt->execute();
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $act) {
                $act['imgs'] = array();
                $act['name'] = to_utf($act['name']);
                $photo_sql = "SELECT `filename` FROM `price_product_pictures` WHERE `productID`=:pid";
                $photo_stmt = $this->db->prepare($photo_sql);
                $photo_stmt->bindValue('pid', $act['productID']);
                $photo_stmt->execute();
                foreach ($photo_stmt->fetchAll(PDO::FETCH_NUM) as $p) {
                    array_push($act['imgs'], "http://uchot.ru/data/small/" . $p[0]);
                }
                array_push($res, $act);
            }
        } catch (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            $x = ["text" => $e->getMessage(), "code" => $e->getCode()];
            $res['error'] = $x;
            $res['ok'] = false;
            return $res;
        }

        return $res;
    }

    function get_acts()
    {
        return array_merge($this->get_discount(), $this->get_gift());
    }

    function get_discount()
    {
        $res = array();
        try {
            $stmt = $this->db->prepare($this->sql_discount);
            $stmt->execute();
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $act) {
                $act['description'] = to_utf($act['description']);
                array_push($res, $act);
            }

        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        return $res;
    }

    function get_gift()
    {
        $res = array();
        try {
            $stmt = $this->db->prepare($this->sql_gift);
            $stmt->execute();
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $act) {
                $act['description'] = to_utf($act['description']);
                $photostmt = $this->db->prepare("SELECT `image` FROM `actions_photos` WHERE `actId`=:id");
                $photostmt->bindParam(':id', $act['id'], PDO::PARAM_STR);
                $photostmt->execute();
                $act['images'] = $photostmt->fetchAll(PDO::FETCH_COLUMN);
                unset($act['id']);
                array_push($res, $act);
            }

        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        return $res;
    }
}