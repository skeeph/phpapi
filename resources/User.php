<?php

class User
{
    private $sql = "SELECT `patronymic`,`fname` AS `name`,`surname`,`points`,`email` FROM users WHERE id=:id";
    private $sql_list = "SELECT `id`,`name` FROM `product_lists` WHERE `userId`=:id";
    private $sql_product_in_list = "SELECT `productId` FROM `list_product` WHERE `listId`=:id";
    private $sql_emotions = "SELECT `shopId` AS `id`,`emotion` FROM `shop_emotions` WHERE `userId`=:id";
    private $name = null;
    private $surname = null;
    private $patronymic = null;

    function __construct($id)
    {
        try {
            $this->db = getConnection();
            $this->id = $id;
            $this->db->setAttribute(PDO::MYSQL_ATTR_INIT_COMMAND, 'SET NAMES cp1251');
            $this->db->exec('SET NAMES cp1251');
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
    }

    function set_name($name)
    {
        $this->name = $name;
    }

    function set_surname($surname)
    {
        $this->surname = $surname;
    }

    function set_patronymic($p)
    {
        $this->patronymic = $p;
    }

    static function constructByEmail($email)
    {
        try {
            $db = getConnection();
            $stmt = $db->prepare('SELECT `id` FROM `users` WHERE `email`=:email');
            $stmt->bindValue(':email', $email);
            $stmt->execute();
            if ($stmt->rowCount() > 0) {
                return new User($stmt->fetchAll(PDO::FETCH_ASSOC)[0]['id']);
            } else {
                $stmt = $db->prepare('INSERT INTO `users`(`email`) VALUES (:email)');
                $stmt->bindValue(':email', $email);
                $stmt->execute();
                $id = $db->lastInsertId();
                return new User($id);
            }
        } catch (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
    }

    function save()
    {
        try {
            $stmt = $this->db->prepare('UPDATE `users` SET `patronymic`=:p,`fname`=:name,`surname`=:surname WHERE `id`=:id');
            $stmt->bindValue(':p', $this->patronymic);
            $stmt->bindValue(':name', $this->name);
            $stmt->bindValue(':surname', $this->surname);
            $stmt->bindValue(':id', $this->id);
            $stmt->execute();
        } catch (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }

    }

    function __destruct()
    {
        $this->db = null;
    }

    function getInfo()
    {
        $res = array();
        try {
            $stmt = $this->db->prepare($this->sql);
            $stmt->bindParam(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            $user = $stmt->fetchAll(PDO::FETCH_ASSOC)[0];
            foreach ($user as $key => $value) {
                $res[$key] = to_utf($value);
            }
        } catch (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        return $res;
    }

    function getList()
    {
        $res = array();
        try {
            $stmt = $this->db->prepare($this->sql_list);
            $stmt->bindParam(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            $lists = $stmt->fetchAll(PDO::FETCH_ASSOC);
            foreach ($lists as $list) {
                $a = array();
                foreach ($list as $key => $value) {
                    $a[$key] = $value;
                }
                $stmt_product = $this->db->prepare($this->sql_product_in_list);
                $stmt_product->bindParam(':id', $list['id'], PDO::PARAM_STR);
                $stmt_product->execute();
                $a['products'] = $stmt_product->fetchAll(PDO::FETCH_COLUMN);
                array_push($res, $a);
            }
        } catch (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        return $res;
    }

    function getEmotions()
    {
        $res = array();
        try {
            $stmt = $this->db->prepare($this->sql_emotions);
            $stmt->bindParam(':id', $this->id, PDO::PARAM_STR);
            $stmt->execute();
            $res = $stmt->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        return $res;
    }

    function getToken()
    {
        $token = array(
            'id' => $this->id,
            'exp' => time() + APY_KEY_LIFETIME
        );
        $jwt = \Firebase\JWT\JWT::encode($token, SECRET_KEY);
        return $jwt;
    }

    static function validate_token($token)
    {
        try {
            \Firebase\JWT\JWT::$leeway = APY_KEY_LIFETIME;
            $data = \Firebase\JWT\JWT::decode($token, SECRET_KEY, array('HS256'));
            $user = new User($data->id);
            return $user;
        } catch (Exception $e) {
            header('Content-type:application/json');
            echo json_encode(array('Error' => true, 'msg' => $e->getFile() . ' ' . $e->getMessage()));
            exit(401);
        }
    }
}