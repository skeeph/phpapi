<?php

class Products
{
    private $sql_deleted = 'SELECT `productId` FROM `deleted_products` WHERE `timestamp`>:ts';
    private $sql_new = 'SELECT `productID`,`categoryID`,`name`,`description`,`sort_order` FROM `price_products` WHERE `added`>:ts';
    private $sql_changed = 'SELECT `productID`,`categoryID`,`name`,`description`,`sort_order` FROM `price_products` WHERE `changed`>:ts';

    function __construct()
    {
        $this->db = getConnection();
    }

    function __destruct()
    {
        $this->db = null;
    }

    function get_new($ts)
    {
        $res = array();
        try {

            $stmt = $this->db->prepare($this->sql_new);
            $stmt->bindValue(':ts', $ts);
            $stmt->execute();
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $item) {
                $tempItem = array();
                $tempItem['id'] = $item['productID'];
                $tempItem['parent'] = $item['categoryID'];
                $tempItem['name'] = to_utf($item['name']);
                $tempItem['info'] = to_utf($item['description']);
                $tempItem['showPriority'] = $item['sort_order'];
                $ratingSt = $this->db->prepare('SELECT AVG(rating) FROM `product_rating` WHERE `productId`=:id');
                $ratingSt->bindValue(':id', $tempItem['id']);
                $ratingSt->execute();
                $tempItem['rating'] = $ratingSt->fetchAll()[0][0];

                $stim = $this->db->prepare('SELECT `image` FROM `product_images` WHERE `productId`=:id');
                $stim->bindValue(':id', $tempItem['id']);
                $stim->execute();
                $tempItem['photos'] = $stim->fetchAll(PDO::FETCH_NUM);
                array_push($res, $tempItem);
            }
        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        return $res;
    }

    function get_changed($ts)
    {
        $res = array();
        try {
            $stmt = $this->db->prepare($this->sql_changed);
            $stmt->bindValue(':ts', $ts);
            $stmt->execute();
            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $item) {
                $tempItem = array();
                $tempItem['id'] = $item['productID'];
                $tempItem['parent'] = $item['categoryID'];
                $tempItem['name'] = to_utf($item['name']);
                $tempItem['info'] = to_utf($item['description']);
                $tempItem['showPriority'] = $item['sort_order'];
                $ratingSt = $this->db->prepare('SELECT AVG(rating) FROM `product_rating` WHERE `productId`=:id');
                $ratingSt->bindValue(':id', $tempItem['id']);
                $ratingSt->execute();
                $tempItem['rating'] = $ratingSt->fetchAll()[0][0];

                $stim = $this->db->prepare('SELECT `image` FROM `product_images` WHERE `productId`=:id');
                $stim->bindValue(':id', $tempItem['id']);
                $stim->execute();
                $tempItem['photos'] = $stim->fetchAll(PDO::FETCH_NUM);
                array_push($res, $tempItem);
            }

        } catch (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        } finally {
            $db = null;
        }
        return $res;
    }

    function get_deleted($ts)
    {
        $res = array();
        try {
            $statement = $this->db->prepare($this->sql_deleted);
            $statement->bindValue(':ts', $ts);
            $statement->execute();
            foreach ($statement->fetchAll(PDO::FETCH_ASSOC) as $item) {
                array_push($res, $item['productId']);
            };
        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }

        return $res;
    }
}