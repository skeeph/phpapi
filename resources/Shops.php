<?php


class Shops
{
    private $sql_delete = 'SELECT `shopId` FROM `deleted_shops` WHERE `timestamp`>:ts';
    private $sql_new = 'SELECT  `id` ,  `name` ,  `description` ,  `picture` FROM  `price_magazin` WHERE  `added`>:added';
    private $sql_changed = 'SELECT  `id` ,  `name` ,  `description` ,  `picture` FROM  `price_magazin` WHERE  `changed`>:changed';

    function __construct()
    {
        try {
            $this->db = getConnection();
        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
    }

    function __destruct()
    {
        $this->db = null;
    }

    function get_new($ts)
    {
        $res = array();
        try {
            $stmt = $this->db->prepare($this->sql_new);
            $stmt->bindValue(':added', $ts);
            $stmt->execute();

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $item) {
                $tempItem = array();
                $tempItem['id'] = $item['id'];
                $tempItem['logo'] = $item['picture'];
                $tempItem['name'] = to_utf($item['name']);
                $tempItem['description'] = to_utf($item['description']);
                $coordSql = 'SELECT `mapx`,`mapy` FROM `price_magazin_sub` WHERE `parent`=:id';
                $s = $this->db->prepare($coordSql);
                $s->bindValue(':id', $item['id']);
                $s->execute();
                $coords = array();
                foreach ($s->fetchAll(PDO::FETCH_ASSOC) as $c) {
                    array_push($coords, array('latitue' => $c['mapx'], 'longitude' => $c['mapy']));
                }
                $tempItem['coordinates'] = $coords;
                $ss = $this->db->prepare('SELECT AVG(rating) FROM `shop_rating` WHERE `shopId`=:id');
                $ss->bindValue(':id', $item['id']);
                $ss->execute();
                $tempItem['rating'] = $ss->fetchAll()[0][0];

                $stim = $this->db->prepare('SELECT `image` FROM `shop_images` WHERE `shopId`=:id');
                $stim->bindValue(':id', $item['id']);
                $stim->execute();
                $photos=array();
                foreach ($stim->fetchAll(PDO::FETCH_NUM) as $p){
                    array_push($photos, 'http://uchot.ru/data/magazin/'.$p[0]);
                }
                $tempItem['photos'] = $photos;
                array_push($res, $tempItem);
            }
        } catch (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        } finally {
            $db = null;
        }
        return $res;
    }

    function get_changed($ts)
    {
        $res = array();
        try {
            $stmt = $this->db->prepare($this->sql_changed);
            $stmt->bindValue(':changed', $ts);
            $stmt->execute();

            foreach ($stmt->fetchAll(PDO::FETCH_ASSOC) as $item) {
                $tempItem = array();
                $tempItem['id'] = $item['id'];
                $tempItem['logo'] = $item['picture'];
                $tempItem['name'] = to_utf($item['name']);
                $tempItem['description'] = to_utf($item['description']);
                $coordSql = 'SELECT `mapx`,`mapy` FROM `price_magazin_sub` WHERE `parent`=:id';
                $s = $this->db->prepare($coordSql);
                $s->bindValue(':id', $item['id']);
                $s->execute();
                $coords = array();
                foreach ($s->fetchAll(PDO::FETCH_ASSOC) as $c) {
                    array_push($coords, array('latitue' => $c['mapx'], 'longitude' => $c['mapy']));
                }
                $tempItem['coordinates'] = $coords;
                $ss = $this->db->prepare('SELECT AVG(rating) FROM `shop_rating` WHERE `shopId`=:id');
                $ss->bindValue(':id', $item['id']);
                $ss->execute();
                $tempItem['rating'] = $ss->fetchAll()[0][0];
                $stim = $this->db->prepare('SELECT `image` FROM `shop_images` WHERE `shopId`=:id');
                $stim->bindValue(':id', $item['id']);
                $stim->execute();
                $photos=array();
                foreach ($stim->fetchAll(PDO::FETCH_NUM) as $p){
                    array_push($photos, 'http://uchot.ru/data/magazin/'.$p);
                }
                $tempItem['photos'] = $photos;
                array_push($res, $tempItem);
            }
        } catch (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        } finally {
            $db = null;
        }
        return $res;
    }

    function get_deleted($ts)
    {
        $res = array();
        try {
            $statement = $this->db->prepare($this->sql_delete);
            $statement->bindValue(':ts', $ts);
            $statement->execute();
            foreach ($statement->fetchAll(PDO::FETCH_ASSOC) as $item) {
                array_push($res, $item['shopId']);
            };
        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        return $res;
    }
}