<?php
function getConnection()
{
    $dbhost = DBHOST;
    $dbuser = DBUSER;
    $dbpass = DBPASS;
    $dbname = DBNAME;
    $dbh = new PDO("mysql:host=$dbhost;dbname=$dbname;charset=cp1251", $dbuser, $dbpass);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    return $dbh;
}

function checkKey()
{
    $app = \Slim\Slim::getInstance();
    $header = $app->request->headers("Authorization");
    if (strlen($header) == 0) {
        header('Content-Type: application/json');
        $err = array('error' => True, 'status' => 400, 'msg' => 'API key is required');
        echo json_encode($err);
        exit(401);
    }
    try {
        $arr = explode(' ', $header);
        $token = $arr[1];
    } catch (Exception $e) {
        header('Content-Type: application/json');
        $err = array('error' => True, 'status' => 400, 'msg' => 'Invalid auth header format');
        echo json_encode($err);
        exit(401);
    }

    return $token;
}

function strToDatetime($timestamp)
{
    if (!preg_match("/^\d{10,10}$/", $timestamp)) {
        header('Content-Type: application/json');
        $err = array('error' => True, 'status' => 400, 'msg' => 'Invalid timestamp format. Please use \'ddMMyyyyhh\'');
        echo json_encode($err);
        exit(400);
    }
    $date = new DateTime();
    $day = (int)substr($timestamp, 0, 2);
    $month = (int)substr($timestamp, 2, 2);
    $year = (int)substr($timestamp, 4, 4);
    $hour = (int)substr($timestamp, 8, 2);
    $date->setDate($year, $month, $day);
    $date->setTime($hour, 0, 0);
    return $date;
}

function to_utf($value)
{
    return iconv('cp1251', 'utf-8', $value);
}


function array_size($arr)
{
    $serializedFoo = serialize($arr);
    if (function_exists('mb_strlen')) {
        $size = mb_strlen($serializedFoo, 'utf8');
    } else {
        $size = strlen($serializedFoo);
    }
    return $size / 1024 / 1024;
}

function generateRandomString($length = 10)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function chunkArray($ar)
{
    global $stor;
    $res = array();
    foreach ($ar as $item => $value) {
        $res[$item] = array();
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $res[$item][$k] = array();
            }
        } else {
            $res[$item] = $value;
        }
    }

    foreach ($ar as $item => $value) {

        if (is_array($value)) {

            foreach ($value as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $key => $vv) {
                        if (array_size($res) < THRESHOLD) {
                            $res[$item][$k][$key] = $vv;
                            unset($ar[$item][$k][$key]);
                        } else {
                            $randstr = generateRandomString();
                            $stor->set($randstr, $ar);
                            $res['next'] = $randstr;
                            return $res;
                        }
                    }
                } else {
                    $res[$item][$k] = $v;
                }
            }
        } else {
            $res[$item] = $value;
        }
    }
    $res['next'] = false;
    return $res;
}

function send_push($title, $subtitle, $message, $ticker)
{
    try {
        $db = getConnection();
        $stmt = $db->prepare('SELECT `regId` FROM `gcm_registrations_ids`');
        $stmt->execute();
        $registrationIds = $stmt->fetchAll(PDO::FETCH_COLUMN);
    } catch
    (PDOException $e) {
        error_log($e->getMessage(), 3, '/var/tmp/php.log');
        header('Content-Type: application/json');
        echo json_encode(array('error' => $e->getMessage()));
        exit(500);
    }

    $msg = array
    (
        'message' => $message,
        'title' => $title,
        'subtitle' => $subtitle,
        'tickerText' => $ticker,
        'vibrate' => 1,
        'sound' => 1
    );

    $fields = array
    (
        'registration_ids' => $registrationIds,
        'data' => $msg
    );

    $headers = array
    (
        'Authorization: key=' . API_KEY,
        'Content-Type: application/json'
    );

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, 'https://android.googleapis.com/gcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
    $result = curl_exec($ch);
    curl_close($ch);

    return $result;
}

function hashPass($pass, $salt = "someDefaultSalt")
{
    $hash = $salt;
    $hash .= hash ( 'sha512',$salt . $pass,  false );
    return $hash;
}