<?php


function getLastTimestamp($id, $curTimestamp)
{
    $mysqlformat = $curTimestamp->format("Y-m-d H:i:s");
    $selectLastSql = 'SELECT `time` FROM `last_time` WHERE `userId`=:id';
    $numrow = 0;
    $time = null;
    //Получение времени последнего обращения пользователя к апи
    try {
        $db = getConnection();
        $statement = $db->prepare($selectLastSql);
        $statement->bindValue(':id', $id);
        $statement->execute();
        $numrow = $statement->rowCount();
        if ($numrow == 1) {
            $f = $statement->fetch(PDO::FETCH_ASSOC);
            $time = $f['time'];
        }
        $db = null;
    } catch
    (PDOException $e) {
        error_log($e->getMessage(), 3, '/var/tmp/php.log');
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    };

    if ($numrow == 0) {
        //Вставка последнего времени обращения
        $sqlInsertLat = 'INSERT INTO `last_time` (userId,time) VALUES (:id,:timestr)';
        try {
            $db = getConnection();
            $statement = $db->prepare($sqlInsertLat);
            $statement->bindValue(':id', $id);
            $statement->bindValue(':timestr', $mysqlformat);
            $statement->execute();
            $db = null;
        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        $res = '1980-01-01 00:00:00';
    } else {
        $sqlInsertLat = 'UPDATE `last_time` SET `time`=:timestr WHERE `userId`=:id';
        try {
            $db = getConnection();
            $statement = $db->prepare($sqlInsertLat);
            $statement->bindValue(':id', $id);
            $statement->bindValue(':timestr', $mysqlformat);
            $statement->execute();
            $db = null;
        } catch
        (PDOException $e) {
            error_log($e->getMessage(), 3, '/var/tmp/php.log');
            echo '{"error":{"text":' . $e->getMessage() . '}}';
        }
        $res = $time;
    }
    return $res;
}


function deletetime()
{
    $sql = 'DELETE FROM `last_time` WHERE 1';
    try {
        $db = getConnection();
        $statement = $db->prepare($sql);
        $statement->execute();
        $db = null;
    } catch
    (PDOException $e) {
        error_log($e->getMessage(), 3, '/var/tmp/php.log');
        echo '{"error":{"text":' . $e->getMessage() . '}}';
    }
}